/*  Compares the sum of sub_total in the fact_aw_sales_order_item table 
    with valid values for 2012 and 2013 years. */
with
    test_subtotal_qty as (
        select
            sum(case when order_date between '2012-01-01' and '2013-12-31' then sub_total end) as sub_total
        from {{ref ('fact_aw_sales_order_item')}}
    )
    , validation as (
        select
            case
                when
                    sub_total = 77632947.64919032
                then 0
                else 1
            end as error
        from test_subtotal_qty
    )
select *
from validation
where error = 1