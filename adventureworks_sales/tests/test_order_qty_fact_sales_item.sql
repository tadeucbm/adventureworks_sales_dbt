/*  Compares the sum of order_qty in the fact_aw_sales_order_item table 
    with valid values for for 2012 and 2013 years. */
with
    test_order_qty as (
        select
            sum(case when order_date between '2012-01-01' and '2013-12-31' then order_qty end) as order_qty
        from {{ref ('fact_aw_sales_order_item')}}
    )
    , validation as (
        select
            case
                when
                    order_qty = 200367.0
                then 0
                else 1
            end as error
        from test_order_qty
    )
select *
from validation
where error = 1