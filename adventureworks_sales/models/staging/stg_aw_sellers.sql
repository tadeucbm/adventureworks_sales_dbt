with 
    sellers as (
        select
            cast(businessentityid as string) as salesperson_id
            , cast(territoryid as string) as territory_id
            , cast(modifieddate as timestamp) as modified_date
        from {{ source('raw_sales', 'salesperson') }}
    )
    , salesterritory as (
        select
            cast(territoryid as string) as territory_id
            , case
                when countryregioncode = 'US'
                then concat(name, ' US')
                else name
            end as region
            , countryregioncode as country_code
            , "group" as continent
        from {{ source('raw_sales', 'salesterritory') }}
    )
    , person as (
        select
            cast(businessentityid as string) as person_id
            , concat(firstname, ' ', lastname) as complete_name
            , firstname as first_name
            , lastname as last_name
        from {{ source('raw_person', 'person') }}
    )
    , joined as (
        select
            se.salesperson_id
            , pe.complete_name
            , pe.first_name
            , pe.last_name
            , st.region
            , st.country_code
            , st.continent
            , se.modified_date
        from sellers se
        left join salesterritory st
            on se.territory_id = st.territory_id
        left join person pe
            on se.salesperson_id = pe.person_id
    )
select *
from joined
