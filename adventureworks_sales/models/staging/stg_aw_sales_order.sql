with 
    salesorderheader as (
        select
            cast(salesorderid as string) as sales_order_id
            , cast(territoryid as string) as territory_id
            , cast(customerid as string) as customer_id
            , cast(salespersonid as string) as salesperson_id
            , cast(orderdate as timestamp) as order_date
            , cast(modifieddate as timestamp) as modified_date
            , case
                when onlineorderflag = true
                then 'Sim'
                else 'Não'
            end as is_online_order
            , taxamt as taxa_mt
            , freight
            , subtotal as sub_total
            , totaldue as total_due
        from {{source('raw_sales', 'salesorderheader')}}
    ) 
    , salesterritory as (
        select
            cast(territoryid as string) as territory_id
            , case
                when countryregioncode = 'US'
                then concat(name, ' US')
                else name
            end as region
            , countryregioncode as country_code
            , st.group as continent
        from {{ source('raw_sales', 'salesterritory')}} st
    )
    , customers as (
        select
            store_id
            , customer_id
        from {{ ref('stg_aw_customers') }}
    )
    , joined as (
        select
            soh.sales_order_id
            , soh.salesperson_id
            , soh.customer_id
            , co.store_id
            , soh.order_date
            , st.region
            , st.country_code
            , st.continent
            , soh.is_online_order
            , soh.taxa_mt
            , soh.freight
            , soh.sub_total
            , soh.total_due
            , soh.modified_date
        from salesorderheader soh
        left join salesterritory st
            on soh.territory_id = st.territory_id
        left join customers co
            on soh.customer_id = co.customer_id
    )
select *
from joined