with
    store as (
        select
            cast(name as string) as store_name	
            , cast(businessentityid as string) as store_id
            , cast(modifieddate as timestamp) as modified_date
        from {{ source('raw_sales', 'store') }}
    )
select *
from store