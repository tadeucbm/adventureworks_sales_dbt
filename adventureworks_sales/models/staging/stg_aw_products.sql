with
    products as (
        select
            cast(productid as string) as product_id
            , cast(productsubcategoryid as string) as product_subcategory_id
            , name as product_name
            , size 
            , class
            , color
            , style
            , cast(listprice as numeric) as list_price
            , standardcost as standard_cost
            , cast(cast(sellstartdate as timestamp) as date) as sell_start_date
            , cast(cast(sellenddate as timestamp) as date) as sell_end_date
            , case
                when sellenddate is null
                then 'Sim'
                else 'Não'
            end as is_active
            , cast(modifieddate as timestamp) as modified_date
        from {{ source('raw_production', 'product') }}
    )

    , product_subcategory as (
        select
            cast(productsubcategoryid as string) as product_subcategory_id
            , cast(productcategoryid as string) as product_category_id
            , name as subcategory_name
        from {{ source('raw_production', 'productsubcategory') }}
    )

    , product_category as (
        select
            cast(productcategoryid as string) as product_category_id
            , case
                when name = 'Clothing' then 'Vestuário'
                when name = 'Components' then 'Componentes'
                when name = 'Accessories' then 'Acessórios'
                when name = 'Bikes' then 'Bicicletas'
                else null
            end as category_name
        from {{ source('raw_production', 'productcategory') }}
    )

    , joined as (
        select
            product_id
            , product_name
            , size 
            , class
            , color
            , style
            , subcategory_name
            , category_name
            , list_price
            , standard_cost
            , sell_start_date
            , sell_end_date
            , is_active
            , modified_date
        from products
        left join product_subcategory
            on products.product_subcategory_id = product_subcategory.product_subcategory_id
        left join product_category
            on product_subcategory.product_category_id = product_category.product_category_id
    )
select *
from joined
