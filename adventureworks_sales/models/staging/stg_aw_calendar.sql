with
    orders_dates as (
        select distinct order_date as metric_date
        from {{ ref('stg_aw_sales_order_item') }}
    )
    , dates as (
        select distinct
            metric_date
            , extract(day from metric_date) as dayofmonth
            , extract(month from metric_date) as month
            , extract(year from metric_date) as year
            , extract(quarter from metric_date) as quarter
            , extract(dayofyear from metric_date) as dayofyear
            , extract(week from metric_date) as isoweek
            , extract(dayofweek from metric_date) as dayofweek_number
            , case
                when extract(dayofweek from metric_date) = 1 then 'Domingo'
                when extract(dayofweek from metric_date) = 2 then 'Segunda'
                when extract(dayofweek from metric_date) = 3 then 'Terça'
                when extract(dayofweek from metric_date) = 4 then 'Quarta'
                when extract(dayofweek from metric_date) = 5 then 'Quinta'
                when extract(dayofweek from metric_date) = 6 then 'Sexta'
                when extract(dayofweek from metric_date) = 7 then 'Sábado'
            end as dayofweek
        from orders_dates
    )
select *
from dates