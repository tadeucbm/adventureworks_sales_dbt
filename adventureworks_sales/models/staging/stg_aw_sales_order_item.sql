with 
    salesorderheader as (
        select
            cast(salesorderid as string) as sales_order_id
            , cast(territoryid as string) as territory_id
            , cast(customerid as string) as customer_id
            , cast(salespersonid as string) as salesperson_id
            , cast(orderdate as timestamp) as order_date
            , cast(modifieddate as datetime) as modified_date
            , case
                when onlineorderflag = true
                then 'Online'
                else 'Física'
            end as is_online_order
            , totaldue as total_due
        from {{source('raw_sales', 'salesorderheader')}}
    ) 
    , salesorderdetail as (
        select
            cast(productid as string) as product_id
            , cast(salesorderid as string) as sales_order_id
            , cast(salesorderdetailid as string) as sales_order_detail_id
            , cast(modifieddate as timestamp) as modified_date
            , orderqty as order_qty
            , unitprice as unit_price
            , orderqty * unitprice as sub_total
            , unitpricediscount as unit_price_discount
        from {{source('raw_sales', 'salesorderdetail')}}
    )
    , salesterritory as (
        select
            cast(territoryid as string) as territory_id
            , case
                when name = 'Canada' then 'Canada'
                when name = 'France' then 'França'
                when name = 'Germany' then 'Alemanha'
                when name = 'Australia' then 'Austrália'
                when name = 'Central' then 'EUA Central'
                when name = 'Northeast' then 'EUA Nordeste'
                when name = 'Northwest' then 'EUA Noroeste'
                when name = 'Southeast' then 'EUA Sudeste'
                when name = 'Southwest' then 'EUA Sudoeste'
                when name = 'United Kingdom' then 'Reino Unido'
                else name
            end as region
            , case
                when countryregioncode = 'CA' then 'CA'
                when countryregioncode = 'FR' then 'FR'
                when countryregioncode = 'DE' then 'AL'
                when countryregioncode = 'AU' then 'AU'
                when countryregioncode = 'US' then 'EUA'
                when countryregioncode = 'GB' then 'RU'
                else countryregioncode
            end as country_code
            , case 
                when st.group = 'North America' then 'América do Norte'
                when st.group = 'Europe' then 'Europa'
                when st.group = 'Pacific' then 'Oceania'
                else st.group
            end as continent
        from {{ source('raw_sales', 'salesterritory')}} st
    )
    , customers as (
        select
            store_id
            , customer_id
        from {{ ref('stg_aw_customers') }}
    )
    , joined as (
        select
            sod.sales_order_detail_id
            , soh.sales_order_id
            , soh.salesperson_id
            , soh.customer_id
            , sod.product_id
            , co.store_id
            , soh.order_date
            , st.region
            , st.country_code
            , st.continent
            , soh.is_online_order
            , sod.order_qty
            , sod.sub_total
            , sod.unit_price
            , sod.unit_price_discount
            , soh.total_due
            , sod.modified_date
        from salesorderheader soh
        left join salesorderdetail sod
            on soh.sales_order_id = sod.sales_order_id
        left join salesterritory st
            on soh.territory_id = st.territory_id
        left join customers co
            on soh.customer_id = co.customer_id
    )
select *
from joined