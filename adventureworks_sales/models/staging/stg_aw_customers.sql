with
    customer as (
        select
            cast(customerid as string) as customer_id
            , cast(storeid as string) as store_id
            , cast(personid as string) as person_id
            , cast(territoryid as string) as territory_id
            , cast(modifieddate as timestamp) as modified_date
        from {{ source('raw_sales', 'customer')}}
    )
    , person as (
        select
            cast(businessentityid as string) as  person_id
            , concat(firstname, ' ', lastname) as complete_name
            , firstname as first_name
            , lastname as last_name
            , cast(persontype as string) as person_type
            , cast(modifieddate as timestamp) as modified_date
            , cast(emailpromotion as numeric) as email_promotion
        from {{ source('raw_person', 'person')}}
    )
    , salesterritory as (
        select
            cast(territoryid as string) as territory_id
            , case
                when countryregioncode = 'US'
                then concat(name, ' US')
                else name
            end as region
            , countryregioncode as country_code
            , "group" as continent
        from {{ source('raw_sales', 'salesterritory')}}
    )
    , joined as (
        select 
            co.customer_id
            , co.person_id
            , co.store_id
            , pe.complete_name
            , pe.first_name
            , pe.last_name
            , pe.person_type
            , pe.email_promotion
            , st.region
            , st.country_code
            , st.continent
            , co.modified_date
        from customer co
        left join person pe
            on co.person_id = pe.person_id
        left join salesterritory st
            on co.territory_id = st.territory_id
    )
select *
from joined