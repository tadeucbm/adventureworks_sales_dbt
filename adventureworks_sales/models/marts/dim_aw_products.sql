with
    final as (
        select
            {{ dbt_utils.surrogate_key(['product_id']) }} as product_sk
            , product_id
            , product_name
            , category_name
            , subcategory_name
            , size
            , class
            , color
            , style
            , list_price
            , standard_cost
            , sell_start_date
            , sell_end_date
            , is_active
            , modified_date
        from {{ ref('stg_aw_products')}}
    )
select *
from final