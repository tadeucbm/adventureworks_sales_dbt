with
    dim_dates as (
        select
            date_sk
            , metric_date
        from {{ ref('dim_aw_calendar') }}
    )
    , dim_stores as (
        select
            store_sk
            , store_id
        from {{ ref('dim_aw_stores') }}
    )
    , dim_customers as (
        select
            customer_sk
            , customer_id
        from {{ ref('dim_aw_customers') }}
    )
    , dim_sellers as (
        select
            seller_sk
            , salesperson_id
        from {{ ref('dim_aw_sellers') }}
    )
    , dim_products as (
        select
            product_sk
            , product_id
        from {{ ref('dim_aw_products') }}
    )
    , stg_sales_order_item as (
        select
            sales_order_detail_id
            , sales_order_id
            , salesperson_id
            , customer_id
            , product_id
            , store_id
            , order_date
            , region
            , country_code
            , continent
            , is_online_order
            , order_qty
            , sub_total
            , unit_price
            , unit_price_discount
            , total_due
            , modified_date
        from {{ ref('stg_aw_sales_order_item') }}
    )
    , final as (
        select distinct
            {{ dbt_utils.surrogate_key(['sales_order_detail_id']) }} as order_item_sk
            , {{ dbt_utils.surrogate_key(['sales_order_id']) }} as order_sk
            , dim_dates.date_sk as date_fk
            , dim_stores.store_sk as store_fk
            , dim_customers.customer_sk as customer_fk
            , dim_sellers.seller_sk as seller_fk
            , dim_products.product_sk as product_fk 
            , soi.order_date
            , soi.region
            , soi.country_code
            , soi.continent
            , soi.is_online_order
            , soi.order_qty
            , soi.sub_total
            , soi.unit_price
            , soi.unit_price_discount
            , soi.total_due
            , soi.modified_date
        from stg_sales_order_item soi
        left join dim_dates 
            on soi.order_date = dim_dates.metric_date
        left join dim_stores 
            on soi.store_id = dim_stores.store_id
        left join dim_customers 
            on soi.customer_id = dim_customers.customer_id
        left join dim_sellers 
            on soi.salesperson_id = dim_sellers.salesperson_id
        left join dim_products 
            on soi.product_id = dim_products.product_id 
    )
select distinct *
from final