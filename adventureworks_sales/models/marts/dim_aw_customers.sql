with
    final as (
        select
            {{ dbt_utils.surrogate_key(['customer_id']) }} as customer_sk
            , customer_id
            , person_id
            , complete_name
            , first_name
            , last_name
            , person_type
            , email_promotion
            , region
            , country_code
            , continent
            , modified_date
        from {{ ref('stg_aw_customers')}}
    )
select *
from final