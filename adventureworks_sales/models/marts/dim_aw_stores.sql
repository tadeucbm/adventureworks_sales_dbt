with 
    final as (
        select
            {{ dbt_utils.surrogate_key(['store_id']) }} as store_sk
            , store_id
            , store_name
            , modified_date
        from {{ ref('stg_aw_stores') }}
    )
select distinct *
from final