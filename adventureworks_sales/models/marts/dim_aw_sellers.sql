with 
    final as (
        select
            {{ dbt_utils.surrogate_key(['salesperson_id']) }} as seller_sk
            , salesperson_id
            , complete_name
            , first_name
            , last_name   
            , region
            , country_code
            , continent
            , modified_date
        from {{ ref('stg_aw_sellers') }}
    )
select *
from final