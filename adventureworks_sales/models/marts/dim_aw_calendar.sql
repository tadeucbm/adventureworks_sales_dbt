with
    dim_date as (
        select *
        from {{ ref('stg_aw_calendar') }}
    )
select
    {{ dbt_utils.surrogate_key(['metric_date']) }} as date_sk
    , metric_date
    , dayofmonth
    , month
    , year
    , quarter
    , dayofyear
    , isoweek
    , dayofweek_number
    , dayofweek
from dim_date