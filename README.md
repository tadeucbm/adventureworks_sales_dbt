# README #

### What is this repository for? ###

Este repositório contém os códigos para a transformação e ingestão de dados do desafio de analytics do Lighthouse. 
Foram usados os dados históricos do setor de vendas da empresa fictícia AdventureWorks. O foco do projeto foi desenvolvedor
o processo completo de ELT, com o objetivo de alimentar um Dashboard no PowerBi

### How do I get set up? ###

Inicialmente, é necessário a criação de um ambiente virtual isolado. Para isso, você pode utilizar o pacote virtualenv no linux. 
Com o pacote instalado, basta rodar o comando virtualenv venv e entrar no ambiente com source venv/bin/activate. Dentro do ambiente virtual, 
instale os pacotes necessários com pip install requirements.txt.   

Para este projeto você deve ter as permissões necessárias para se conectar aos conjuntos de dados do projeto no BigQuery. Com essas permissões,
preencha o arquivo profiles.yml disponível no repositório .dbt da seguinte forma:

****

* Para testar a sua conexão, utilize o comando dbt debug.   

* Se a conexão funcionar, utilize o comando dbt deps para instalar as dependências necessárias.   

* Para rodar o projeto, basta fazer o comando dbt run.   


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact